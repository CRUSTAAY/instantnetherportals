package com.rngservers.instantnetherportals.util;

import net.minecraft.server.v1_13_R2.EntityPlayer;
import net.minecraft.server.v1_13_R2.PlayerAbilities;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class Util113 {
	public Boolean setInvulnerable(Player player, Boolean setting) {
		EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
		try {
			PlayerAbilities playerAbilities = entityPlayer.abilities;
			Field field = playerAbilities.getClass().getDeclaredField("isInvulnerable");
			field.setAccessible(true);
			field.setBoolean(playerAbilities, setting);
			field.setAccessible(false);
			return true;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException event) {
			return false;
		}
	}
}

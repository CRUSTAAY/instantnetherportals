package com.rngservers.instantnetherportals.util;

import com.rngservers.instantnetherportals.Main;
import org.bukkit.entity.Player;

public class Invulnerable {
    private Main plugin;

    public Invulnerable(Main plugin) {
        this.plugin = plugin;
    }

    public void setInvulnerable(Player player, Boolean bool) {
			if (plugin.getServer().getVersion().contains("1.12")) {
				new Util112().setInvulnerable(player, bool);
			} else if (plugin.getServer().getVersion().contains("1.13")) {
				new Util113().setInvulnerable(player, bool);
			} else if (plugin.getServer().getVersion().contains("1.14")) {
				new Util114().setInvulnerable(player, bool);
			} else if (plugin.getServer().getVersion().contains("1.15")) {
				new Util115().setInvulnerable(player, bool);
			} else if (plugin.getServer().getVersion().contains("1.16")) {
				new Util116().setInvulnerable(player, bool);
			} else {
				plugin.getServer().getLogger().info("[InstantNetherPortals] Unsupported Version");
				return;
			}
    }
}

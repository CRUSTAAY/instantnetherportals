package com.rngservers.instantnetherportals;

import com.rngservers.instantnetherportals.util.Invulnerable;
import org.bukkit.plugin.java.JavaPlugin;

import com.rngservers.instantnetherportals.events.Events;

public class Main extends JavaPlugin {
	@Override
	public void onEnable() {
		Invulnerable invulnerable = new Invulnerable(this);
		this.getServer().getPluginManager().registerEvents(new Events(this, invulnerable), this);
	}
}
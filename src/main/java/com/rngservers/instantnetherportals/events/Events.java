package com.rngservers.instantnetherportals.events;

import com.rngservers.instantnetherportals.Main;
import com.rngservers.instantnetherportals.util.Invulnerable;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Events implements Listener {
	private Main plugin;
	private Invulnerable invulnerable;

	public Events(Main plugin, Invulnerable invulnerable) {
		this.plugin = plugin;
		this.invulnerable = invulnerable;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (player.getGameMode().equals(GameMode.CREATIVE)) {
			return;
		}
		if (player.getLocation().getBlock().getType().equals(Material.NETHER_PORTAL)) {
			invulnerable.setInvulnerable(player, true);
			new BukkitRunnable() {
				@Override
				public void run() {
					invulnerable.setInvulnerable(player, false);
				}
			}.runTaskLater(plugin, 3);
		}
	}
}
